#include "dstruct.hpp"
#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <exception>

int main() {

  try {

    using iter = std::istream_iterator< DStruct::DataStruct >;
 
    std::vector< DStruct::DataStruct > DataVector( iter( std::cin ), iter() );

    std::sort( DataVector.begin(), DataVector.end(), DStruct::CompData() );
    std::copy( DataVector.begin(), DataVector.end(), std::ostream_iterator< DStruct::DataStruct >( std::cout, "\n" ) );

    if ( !std::cin.eof() ) {
      std::cerr << "Invalid data in " << ( DataVector.size() + 1 ) << " line\n";
      return 1;
    }
    
  } catch ( const std::exception & e ) {
    std::cerr << e.what() << "\n";
    return 2;
  }

  return 0;
}
