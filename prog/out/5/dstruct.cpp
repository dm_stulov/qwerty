#include "dstruct.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <cctype>


static const int maxKey = 5;
static const int minKey = -5;
static const char spr = ',';

DStruct::DataStruct::DataStruct( int k1, int k2,  const std::string & s): key1(k1), key2(k2), str(s)
{}

std::ostream & DStruct::operator << ( std::ostream & out, const DStruct::DataStruct & data ) 
{
  std::ostream::sentry sentry( out ); 

  if ( sentry ) {
    out << data.key1 << spr << data.key2 << spr << data.str;
  }

  return out;
}

std::istream & DStruct::operator >> ( std::istream & in, DStruct::DataStruct & data )
{
  std::istream::sentry sentry( in );

  if ( sentry ) {
    in >> std::ws;
    
    if ( in.eof() ){
      in.setstate( std::istream::failbit );
      return in;
    }
 
    int key1, key2;
    char spr1, spr2;
    std::string str;

    in >> key1 >> spr1 >> 
          key2 >> spr2 >> std::ws;
    

    if ( !in ) {
      in.clear( std::cin.rdstate() & ~std::ios::eofbit );
      in.setstate( std::ios::failbit );
      return in;
    }

    getline( in, str );
    while( std::isspace( str.back() ) ) {
      str.pop_back();
    }
    
    if ( ( spr1 != spr ) || ( spr2 != spr ) ) {
      in.clear( std::cin.rdstate() & ~std::ios::eofbit );
      in.setstate( std::ios::failbit );
    }

    if ( key1 < minKey || key1 > maxKey ) {
      in.clear( in.rdstate() & ~std::ios::eofbit );
      in.setstate( std::ios::failbit );
    }    

    if ( key2 < minKey || key2 > maxKey ) {
      in.clear( in.rdstate() & ~std::ios::eofbit );
      in.setstate( std::ios::failbit );
    }    
    
    if ( str.empty() ) {
      in.clear( in.rdstate() & ~std::ios::eofbit );
      in.setstate( std::ios::failbit );
    } 

    if ( in ) {
      data = DStruct::DataStruct( key1, key2, str );
    }
  }

  return in;
}


bool DStruct::CompData::operator()( const DStruct::DataStruct & lhs, const DStruct::DataStruct & rhs ) const
{
  if ( lhs.key1 == rhs.key1 ) {
    if ( lhs.key2 == rhs.key2 ) {
      return ( lhs.str.size() < rhs.str.size() );
    } 
    return ( lhs.key2 < rhs.key2 );
  }
  return ( lhs.key1 < rhs.key1 );
}
