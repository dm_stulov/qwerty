#ifndef DSTRUCT_HPP
#define DSTRUCT_HPP

#include <iosfwd>
#include <string>


namespace DStruct {

  struct DataStruct
  {
  public: 
    DataStruct() = default;
    DataStruct( int k1, int k2, const std::string & s); 
    
    friend std::istream & operator >> ( std::istream &, DataStruct & );
    friend std::ostream & operator << ( std::ostream &, const DataStruct & );
    friend class CompData;

  private:
    int         key1;
    int         key2;
    std::string str;
  };  

  std::istream & operator >> ( std::istream &, DataStruct & );
  std::ostream & operator << ( std::ostream & , const DataStruct & );
  
  class CompData
  {
  public: 
    bool operator()( const DataStruct & lhs, const DataStruct & rhs ) const; 
  };

}

#endif
