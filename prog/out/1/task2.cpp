#include "tasks.hpp"
#include <memory>
#include <fstream>
#include <vector>
#include <ostream>


int tasks::task2( int argc,  char const * argv[] )
{
  if ( argc != 1 || argv[0] == nullptr ) {
    std::cerr << "Invalid keys\n";
    return 1;
  }

  std::string filename = argv[0];
  std::ifstream file( filename );

  if( !file ) {
    std::cerr << "The file \"" << filename << "\" is not accessible\n";
    return 1;
  }

  const size_t inc_size_text = 100; // increment
  size_t size_text = 0;

  std::unique_ptr< char, decltype(free)* > text( nullptr, free );
  char * tmp_ptr = nullptr;

  std::vector< char > char_vector;

  while ( file ) { 
    tmp_ptr = (char*)std::realloc( text.get(), size_text + inc_size_text );
    
    if ( tmp_ptr == nullptr ) {
      throw std::bad_alloc();
    } else {
      text.release();  // предотвращает самоуничтожения в следующей строке
      text.reset( tmp_ptr );  
    }

    file.read( text.get() + size_text, inc_size_text );
    size_text += file.gcount();  
  }

  if ( !file.eof() ) {
    std::cerr << "The file \"" << filename << "\" is failed\n";
    return 1;
  }  

  char_vector.assign( text.get(), text.get() + size_text );   
  
  std::cout.write( char_vector.data(), char_vector.size() );
  
  return 0;
}
