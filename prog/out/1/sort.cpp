#include "sort.hpp"
#include <cstring>
#include <stdexcept>

static const char ascendingStr[] = "ascending";
static const char descendingStr[] = "descending";

static const char invalid_direction[] = "Invalid direction: expected ascending/descending";

sort::Direction sort::getDirection( const char * direct ) 
{
  if ( direct == nullptr ) {
    throw std::invalid_argument( invalid_direction );
  }

  if ( std::strcmp( direct, ascendingStr ) == 0 ) {
    return sort::ascending; 
  }
  if ( std::strcmp( direct, descendingStr ) == 0 ) {
    return sort::descending;
  }

  throw std::invalid_argument( invalid_direction );
}
