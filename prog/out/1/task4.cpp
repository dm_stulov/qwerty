#include "sort.hpp"
#include "tasks.hpp"
#include <vector>
#include <cstdlib>
#include <cstring>
#include <ostream>


int tasks::task4(int argc, char const * argv[] ) 
{

  if ( ( argc != 2 ) || ( argv[1] == nullptr ) ) {
    std::cerr << "Invalid arguments: expected ascending/descending and size of vector > 0\n";
    return 1;
  }
  
  sort::Direction direction = sort::getDirection( argv[0] );

  int size = std::atoi( argv[1] );

  if ( size <= 0 ) {
    if ( strcmp( argv[1], "0" ) == 0 ) {
      return 0;
    }
    std::cerr << "Invalid size: expected size of vector > 0\n";
    return 1;    
  }
 
  std::vector<double> double_v( size );
  fillRandom( double_v.data(), double_v.size() );

  detail::printSequence( std::cout, double_v.begin(), double_v.end(), ' ' );

  sort::Sort< sort::SortVectorByIndices >( double_v, direction );
  
  detail::printSequence( std::cout, double_v.begin(), double_v.end(), ' ' );

  return 0;
}

void tasks::fillRandom( double * array, int size ) 
{
  if ( array == nullptr || size == 0 ) {
    return;
  }

  for (int i = 0; i < size; i++) {
    array[i] = 2*(double)rand() / RAND_MAX - 1;
  }
  return;
}
