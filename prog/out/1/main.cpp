#include "tasks.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <stdexcept>


int main( int argc, char const *argv[] )
{
  if ( argc < 2  || argv[1] == nullptr ) {
    std::cerr << "Invalid keys: expected number of the task\n";
    return 1;
  }

  try {
    std::srand(time(nullptr));
    
    int num = std::atoi(argv[1]);
    switch (num) {
      case 1: 
        return tasks::task1( argc - 2, argv + 2 );
      case 2: 
        return tasks::task2( argc - 2, argv + 2 ); 
      case 3: 
        return tasks::task3( argc - 2, argv + 2 );  
      case 4: 
        return tasks::task4( argc - 2, argv + 2 );
      default: 
        std::cerr << "Invalid keys: expected number of the task\n"; return 1;
    }
  } catch ( const std::invalid_argument & e ) {
    std::cerr <<e.what() << "\n";
    return 1;
  } catch ( const std::bad_alloc & e ) {
    std::cerr << "Allocation failed: " << e.what() << "\n";
  } catch ( const std::exception & e) {
    std::cerr <<e.what() << "\n";
  } catch ( ... ) {
    std::cerr << "Unknown error\n";
  }
  return 2;
}
