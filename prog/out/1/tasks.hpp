#ifndef TASKS_HPP
#define TASKS_HPP

#include <ostream>

namespace tasks {
  int task1(int argc, char const *argv[]);

  int task2(int argc, char const *argv[]);

  int task3(int argc, char const *argv[]);

  int task4(int argc, char const *argv[]);
  void fillRandom(double * array, int size );
}

namespace detail {
  template< typename Iterator >
  void printSequence( std::ostream & out, Iterator first, Iterator last, char delimiter ) 
  {
    
    if ( first == last ) {
      return;
    }

    out << *first;

    while ( ++first != last ) {
      out << delimiter << *first;
    }

    out << "\n";
  }
}

#endif
