#include "sort.hpp"
#include "tasks.hpp"
#include <vector>
#include <forward_list>
#include <iostream>


int tasks::task1(int argc, char const *argv[])
{
  if ( argc != 1 ) {
    std::cerr << "Invalid derection: expected ascending/descending\n";
    return 1;
  }
 
  sort::Direction direction = sort::getDirection( argv[0] );

  std::vector<int> v1;

  int value;
  while ( std::cin.good() && ( std::cin >> value ) ) {
      v1.push_back( value ); 
  }
  
  if ( !std::cin.eof() ) {
    std::cerr << "Invalid arguments: expected integer\n";
    return 1;
  }

  std::vector<int> v2( v1.begin(), v1.end() );
  std::forward_list<int> list( v1.begin(), v1.end() ) ;

  sort::Sort<sort::SortVectorByIndices>( v1, direction );
  sort::Sort<sort::SortVectorByAt>( v2, direction );
  sort::Sort<sort::SortForwardListByIter>( list, direction );
  
  if ( v1.empty() ) {
    return 0;
  }
  
  detail::printSequence( std::cout, v1.begin(), v1.end(), ' ' );
  detail::printSequence( std::cout, v2.begin(), v2.end(), ' ' );
  detail::printSequence( std::cout, list.begin(), list.end(), ' ' );

  return 0;
}
