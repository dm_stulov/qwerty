#include "tasks.hpp"
#include <vector>
#include <iostream>


int tasks::task3(int argc, char const **)
{
  if ( argc != 0 ) {
    std::cerr << "Invalid keys: expected nothing\n";
    return 1;
  }

  std::vector<int> int_vector;
  
  int value;
  while ( std::cin.good() && ( std::cin >> value ) && ( value != 0 ) ) {
      int_vector.push_back( value ); 
  }

  if ( value != 0 ) {
    std::cerr << "Invalid arguments: expected integer and 0 at the end\n";
    return 1;
  }

  if ( int_vector.empty() ) {
    return 0;
  } 

  typename std::vector<int>::const_iterator const_iter = int_vector.begin();

  switch ( int_vector.back() ) {
    case 1 :   
      while ( const_iter != int_vector.end() ) {  
        if ( *const_iter % 2 == 0 ) {
          const_iter = int_vector.erase( const_iter );
        } else {
          ++const_iter;
        }
      }
      break;

    case 2 : 
      std::vector<int> single_array = { 1,1,1 };

      while ( const_iter != int_vector.end() ) {
        if ( *const_iter % 3 == 0 ) {
          const_iter = int_vector.insert( ++const_iter, single_array.begin(), single_array.end() );
          const_iter += single_array.size(); 
        } else {
          ++const_iter;
        }
      }
      break;
  }
  
  detail::printSequence( std::cout, int_vector.begin(), int_vector.end(), ' ' );
  
  return 0;
}
