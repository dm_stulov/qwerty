#ifndef SORT_HPP
#define SORT_HPP

#include <utility>

namespace sort {

  template<typename Container>
  class SortVectorByIndices
  {
  public:
    typedef typename Container::value_type value_type;
    typedef typename Container::size_type size_type;
    
    static  size_type begin( Container & )
    {
      return 0;
    }

    static size_type end( Container & c ) 
    {
      return c.size();
    }

    static value_type & value( Container & c, size_type pos ) 
    {
      return c[pos];
    }
  };

  template<typename Container>
  class SortVectorByAt
  {
  public:
    typedef typename Container::value_type value_type;
    typedef typename Container::size_type size_type;
    
    static size_type begin( Container & ) 
    {
      return 0;
    }

    static size_type end( Container & c ) 
    {
      return c.size();
    }

    static value_type & value( Container & c, size_type pos ) 
    {
      return c.at(pos);
    }
  };

  template <typename Container>
  class SortForwardListByIter
  {
  public:
    typedef typename Container::iterator size_type;
    typedef typename Container::value_type value_type;

    static size_type begin( Container & c ) 
    {
      return c.begin();
    }

    static size_type end( Container & c ) 
    {
      return c.end();
    }

    static value_type & value( Container &, const size_type & pos ) 
    {
      return *pos;
    }
  };

  enum Direction {
    ascending = 0,
    descending = 1
  };

  Direction getDirection( const char * direct ); 

  template< template < typename Container > class Strategy, typename Container >
  void Sort( Container &c, Direction direct )
  {
    Strategy< Container > strategy;
    typedef typename Strategy< Container >::size_type size_type;

    for ( size_type i = strategy.begin( c ); i != strategy.end( c ); ++i ) {
      size_type k = i; 
      for (size_type j = i; ( ++j ) != strategy.end(c); ) {
        if ( (strategy.value( c, j ) < ( strategy.value( c, k ) ) ) ^ direct ) {          
          k = j;
        } 
      }
      std::swap(strategy.value( c,i ), strategy.value( c,k ));
    }

    return;
  }
} //namespace sort
#endif
