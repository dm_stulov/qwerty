#ifndef QUEUEUI_HPP
#define QUEUEUI_HPP

#include <string>
#include <iostream>
#include <cctype>
#include <exception>

#include "queue.hpp"

std::istream &myws( std::istream & in );

class QueueUI
{
public:
  QueueUI( queue::QueueWithPriority< std::string > * q ) : queue( q ), in( &std::cin ), out( &std::cout )
  {}

  typedef void( QueueUI::*action )();

  void add();

  void get();

  void accelerate();

private:
  queue::QueueWithPriority< std::string > * queue;
  std::istream * in;
  std::ostream * out;
};


std::istream & operator >> ( std::istream & in, QueueUI::action & act );

std::istream &myws( std::istream & in );

#endif
