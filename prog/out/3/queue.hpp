#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <deque>

namespace queue 
{
  typedef enum
  {
    HIGH,
    NORMAL,
    LOW
  } Priority;

  template < typename T >
  class QueueWithPriority
  {
  public:

    typedef T QueueElement;
    typedef typename std::deque< T >::size_type size_type;
    typedef Priority ElementPriority;

    QueueWithPriority() = default;
    QueueWithPriority( const QueueWithPriority & other ) = default; 
    QueueWithPriority( QueueWithPriority && other ) = default;

    QueueWithPriority( const QueueElement & element, ElementPriority priority ) 
    {
      PutElementToQueue( element, priority );
    }

    template< class InputIt >
    QueueWithPriority( InputIt first, InputIt last, ElementPriority priority ) 
    {
      PutElementToQueue( first, last, priority );
    }
  
    bool empty() 
    {
      return ( dequeHigh.empty() && dequeNormal.empty() && dequeLow.empty() );
    }

    size_type size() 
    {
      return ( dequeHigh.size() + dequeNormal.size() + dequeLow.size() );
    }

    void clear() 
    { 
      dequeHigh.clear();
      dequeNormal.clear();
      dequeLow.clear();
    }

    void PutElementToQueue( const QueueElement & element, ElementPriority priority )
    {
      switch ( priority ) {
        case HIGH : 
          dequeHigh.push_back( element );
          break;
        case NORMAL : 
          dequeNormal.push_back( element );
          break;
        case LOW : 
          dequeLow.push_back( element );
          break;
      }
    } 

    template < class InputIt >
    void PutElementToQueue( InputIt first, InputIt last, ElementPriority priority ) 
    {
      switch (priority) {
        case HIGH : 
          dequeHigh.insert( dequeHigh.end(), first, last );
          break;
        case NORMAL : 
          dequeNormal.insert( dequeLow.end(), first, last );
          break;
        case LOW : 
          dequeLow.insert( dequeLow.end(), first, last );
          break;
      }
    } 

    QueueElement GetElementFromQueue()
    {
      QueueElement element;
      
      if ( !dequeHigh.empty() ) {
        element = dequeHigh.front();
        dequeHigh.pop_front();
      } else if ( !dequeNormal.empty() ) {
        element = dequeNormal.front();
        dequeNormal.pop_front();
      } else if ( !dequeLow.empty() ) {
        element = dequeLow.front();
        dequeLow.pop_front();
      }
      return element;
    } 

    void Accelerate() 
    {
      dequeHigh.insert( dequeHigh.end(), dequeLow.begin(), dequeLow.end() );
      dequeLow.clear();
    }

  private:  
    std::deque< QueueElement > dequeHigh;
    std::deque< QueueElement > dequeNormal;
    std::deque< QueueElement > dequeLow;
  };
}

#endif
