#include <iostream>
#include <list>
#include <iterator>
#include <stdexcept>

int task2()
{
  std::list<int> list;
  std::istream_iterator<int> iter( std::cin );
  for ( int i = 0; ( ( i < 20 ) && ( iter != std::istream_iterator<int>() ) ); i++ ) {
    int value = *iter++;
    if ( ( value > 20 ) || ( value <= 0 ) ) {
      throw std::invalid_argument("Error: the number doesn't belong to the range (0, 20]\n");
    }
    list.push_back( value );
  }

  if ( !std::cin.eof() ) {
    throw std::invalid_argument("Error: expected namber\n");
  }

  std::list<int>::iterator itFirst = list.begin();
  std::list<int>::reverse_iterator itLast = list.rbegin();

  size_t i = list.size();
  while ( i > 1 ) {
    std::cout << *itFirst++ << " " << *itLast++ << " ";
    i-=2;
  }

  if ( i == 1 ) {
    std::cout << *itFirst << " ";
  }
  std::cout << "\n";

  return 0;
}
