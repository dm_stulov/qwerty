#include "queue.hpp"
#include "QueueUI.hpp"

#include <iostream>
#include <iterator>
#include <algorithm>
#include <string>
#include <stdexcept>


int task1() 
{

  queue::QueueWithPriority< std::string > queue;
  QueueUI q( &queue ); 

  using iter = std::istream_iterator< QueueUI::action >;
  std::for_each( iter( std::cin ), iter(), [&q]( QueueUI::action act ) {
      (q.*act)();
  } );

  if ( !std::cin.eof() ) {
    std::cout << "<INVALID COMMAND>\n";
    return 0;
  }

  return 0;
}
