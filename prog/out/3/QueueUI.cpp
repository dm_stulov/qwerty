#include <string>
#include <iostream>
#include <cctype>
#include <exception>

#include "QueueUI.hpp"

std::istream &myws( std::istream & in );

void QueueUI::add() 
{
  std::string priority, data;
  *in >> priority >> myws;
  std::getline( *in, data );

  if ( data.empty() ) {
    in->clear( std::cin.rdstate() & ~std::ios::eofbit );
    in->setstate( std::ios::failbit );
  } 

  if ( priority == "high" ) {
    queue->PutElementToQueue( data, queue::HIGH );

  } else if ( priority == "normal" ) {
    queue->PutElementToQueue( data, queue::NORMAL );

  } else if ( priority == "low" ) {
    queue->PutElementToQueue( data, queue::LOW );

  } else {
    in->clear( std::cin.rdstate() & ~std::ios::eofbit );
    in->setstate( std::ios::failbit );
  } 

  return;
}

void QueueUI::get()
{
  std::string str;
  *in >> myws;

  if ( ( in->peek() != '\n' ) && ( !in->eof() ) ) {
    in->clear( std::cin.rdstate() & ~std::ios::eofbit );
    in->setstate( std::ios::failbit );
    return;   
  }

  if ( queue->empty() ) {
    *out << "<EMPTY>\n";
    return;   
  }

    *out << queue->GetElementFromQueue() << "\n";
    return;
  }

void QueueUI::accelerate() {
  std::string str;
  *in >> myws;

  if ( in->peek() != '\n' ) {
    in->clear( std::cin.rdstate() & ~std::ios::eofbit );
    in->setstate( std::ios::failbit );
    return;   
  }

  queue->Accelerate();
  return;
}

std::istream & operator >> ( std::istream & in, QueueUI::action & act )
{
  std::istream::sentry sentry( in );

  if ( sentry ) {
    in >> std::ws;
    
    if ( in.eof() ){
      in.setstate( std::istream::failbit );
      return in;
    }

    std::string action;
    in >> action;

    if ( action == "add" ) {
      act = &QueueUI::add;
    
    } else if ( action == "get" ) {
      act =  &QueueUI::get;
    
    } else if ( action == "accelerate" ) {
      act =  &QueueUI::accelerate;
    
    } else {
      in.clear( std::cin.rdstate() & ~std::ios::eofbit );
      in.setstate( std::ios::failbit );
    } 
  }
  
  return in;

}

std::istream &myws( std::istream & in )
{

  if ( !in ) {
    return in;
  }

  char c = in.peek();
  while ( in && ( std::isspace( c ) ) && ( c != '\n' ) ) { 
    in.get();
    c = in.peek();
  }

  return in;
}
