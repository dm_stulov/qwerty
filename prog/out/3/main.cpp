#include <iostream>
#include <stdexcept>
#include <cstdlib>

int task1();
int task2();

int main(int argc, char const *argv[])
{
  if ( argc != 2 ) {
    std::cerr << "Invalid key: expected number of the task\n";
    return 1;
  }

  int number = std::atoi( argv[1] );

  try {
    switch( number ) {
      case 1 : return task1();
      case 2 : return task2(); 
    }

  } catch ( const std::invalid_argument & e ) {
    std::cerr << e.what();
    return 1;
  } catch ( const std::exception & e ) {
    return 1;
  } catch ( ... ) {
    std::cerr << "Unknown exception\n";
    return 2;
  }
  
  std::cerr << "Invalid key: expected number of the task\n";
  return 1;
}
