#include "shapes.hpp"
#include <ios>
#include <cmath>
#include <algorithm>
#include <iterator>


std::istream & shapes::operator >> ( std::istream & in, shapes::Point & point ) 
{
  std::istream::sentry sentry( in );  

  shapes::Point tmp(point);

  if ( sentry ) {

    in >> std::ws;
    if ( in.eof() ) {
      in.setstate( std::ios::failbit );
      return in;
    }

    char spr1;
    char spr2;
    char spr3;

    in >> spr1 >> point.x >> spr2 >> point.y >> spr3;

    if ( !in ) {      
      point = tmp;
      in.clear( std::ios::badbit );
      return in;
    }

    if ( ( spr1 != '(' ) || ( spr2 != ';' ) || ( spr3 != ')' ) ) {
      point = tmp;
      in.clear( std::ios::badbit );
    }
  }

  return in;
}


std::ostream & shapes::operator << ( std::ostream & out, const shapes::Point & point ) 
{
  std::ostream::sentry sentry( out ); 

  if ( sentry ) {
    out << "(" << point.x << "; " << point.y << ")";
  }

  return out;
}


std::istream & shapes::operator >> ( std::istream & in, shapes::Shape & shape ) 
{
  std::istream::sentry sentry( in );  

  shapes::Shape tmp( shape );
  shape.clear();
  
  unsigned int number_point;
  shapes::Point point;

  if ( in >> number_point ) {
    for ( unsigned int i = 0; ( i < number_point ) && ( in >> point ); ++i ) {
      shape.push_back(point);
    }
  }

  if ( !sentry ) {
    shape = tmp;
  }

  return in;
}


std::ostream & shapes::operator << ( std::ostream & out, const shapes::Shape & shape ) 
{
  std::ostream::sentry sentry( out ); 

  if ( sentry ) {
    out << shape.size() << " ";
    std::copy( shape.begin(), shape.end(), std::ostream_iterator< shapes::Point >( out, " " ) );
  }

  return out;
}


inline bool shapes::isTriangle( const shapes::Shape & s ) 
{
  if ( s.size() != 3 ) {
    return false;
  }
  //check points to belong to the same straight line
  return ( ( s[1].x - s[0].x ) * ( s[2].y - s[0].y ) != ( s[2].x - s[0].x ) * ( s[1].y - s[0].y ) );
}


inline long long int length(const shapes::Point & p1, const shapes::Point & p2 ) 
{
  return std::pow( ( p1.x - p2.x ), 2 ) + std::pow( ( p1.y - p2.y ), 2 );
}


inline bool shapes::isRectangle( const shapes::Shape & s ) 
{
  if ( s.size() != 4 ) {
    return false;
  }

  if ( length( s[0], s[2] ) != length(s[1], s[3]) ) {
    return false;
  } 

  if ( length(s[0], s[1]) == length(s[2], s[3]) && length(s[1], s[2]) == length(s[0], s[3]) ) {
    return true;
  }

  return false;
}


inline bool shapes::isSquare( const shapes::Shape & s ) 
{
  if ( !isRectangle( s ) ) {
    return false;
  }

  if ( length( s[0], s[1] ) == length( s[1], s[2] ) ) {
    return true;
  }

  return false;
}

shapes::shape_type shapes::getShapeType( const shapes::Shape & s ) 
{
  if ( shapes::isTriangle( s ) ) {
    return shapes::TRIANGLE;
  } else if ( shapes::isSquare( s ) ) {
    return shapes::SQUARE;
  } else if ( shapes::isRectangle( s ) ) {
    return shapes::RECTANGLE;
  } else {
    return shapes::OTHER;
  }
}

bool shapes::cmp(const shapes::Shape & lhs, const shapes::Shape & rhs) 
{
  return ( shapes::getShapeType(lhs) < shapes::getShapeType(rhs) ) ;
}
