#include <iostream> 
#include <cstdlib> 
#include <stdexcept> 

int task1();
int task2();

static const char Incorrect_arguments[] = "Incorrect arguments: expected number to task\n";

int main( int argc, char const *argv[] ) 
{   
  if ( ( argv[1] == nullptr ) || ( argc != 2 ) ) {
    std::cerr << Incorrect_arguments;
    return 1;   
  }   

  int num = std::atoi( argv[1] );
  
  try {
    switch( num ) {
      case 1:
        return task1();
  
      case 2:
        return task2();
    }

    std::cerr << Incorrect_arguments; 
    return 1;

  } catch ( const std::bad_alloc & e ) {
    std::cerr << "Allocation failed: " << e.what() << '\n';   
  } catch ( const std::exception & e) {
    std::cerr << e.what() << '\n';   
  } catch ( ... ) {     
    std::cerr << "Unknown error\n";   
  }   

  return 2; 
}
