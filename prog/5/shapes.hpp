#ifndef SHAPES_HPP
#define SHAPES_HPP

#include <iostream>
#include <vector>

namespace shapes 
{
  typedef enum {
    TRIANGLE,
    SQUARE,
    RECTANGLE,
    OTHER,
  } shape_type;
  
  struct Point
  {
    int x,y;
  };

  using Shape = std::vector< Point >;

  inline bool isTriangle( const shapes::Shape &);
  inline bool isRectangle( const shapes::Shape & );
  inline bool isSquare( const shapes::Shape & );

  shape_type getShapeType( const shapes::Shape &);
  bool cmp( const shapes::Shape &, const shapes::Shape & );

  std::istream & operator >> ( std::istream & , shapes::Point & point );
  std::ostream & operator << ( std::ostream & , const shapes::Point & point );
  std::istream & operator >> ( std::istream & , shapes::Shape & shape );
  std::ostream & operator << ( std::ostream & , const shapes::Shape & shape );
}

#endif
