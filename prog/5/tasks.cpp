#include "shapes.hpp" 
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <unordered_set>
#include <functional>


int task1()
{
  std::cin >> std::ws;
  if ( std::cin.eof() ) {
    return 0;
  }

  using iter = std::istream_iterator< std::string >;

  std::unordered_set< std::string > set;
  set.insert( iter( std::cin ), iter() );

  if ( !std::cin.eof() ) {
    std::cerr << "Incorrect input: expected strings\n";
    return 1;
  }

  std::copy( set.begin(), set.end(), std::ostream_iterator< std::string >( std::cout, "\n" ) );

  return 0;
}


struct SumVerticesSumShapes 
{
  unsigned int Vertices, Triangles, Squares, Rectangles;

  void operator()( const shapes::Shape & );

};

int task2()
{
  
// ---1---reading shapes-----
  using iter = std::istream_iterator< shapes::Shape >;
  
  std::vector< shapes::Shape > SHAPES( iter( std::cin ), iter() );

  if ( !std::cin.eof() ) {
    std::cerr << "Error: Incorrect input data\n";
    return 1;
  }


// --2/3--analyses of shapes-----
  SumVerticesSumShapes quantity = std::for_each( SHAPES.begin(), SHAPES.end(), SumVerticesSumShapes() );


// ---4---removing pentagons-----
  std::vector< shapes::Shape >::iterator new_end = std::remove_if( SHAPES.begin(), SHAPES.end(), [](const shapes::Shape & shape) -> bool 
      {
        return ( shape.size() == 5 ); 
      });

  SHAPES.erase( new_end, SHAPES.end() );


// ---5---create a vector of points-----
  std::vector< shapes::Point > POINTS( SHAPES.size() );
  std::transform( SHAPES.begin(), SHAPES.end(), POINTS.begin(), []( const shapes::Shape & shape ) -> shapes::Point
      {
        return shape.front();
      } );


// ---6---sorting shapes-----
  std::sort( SHAPES.begin(), SHAPES.end(), std::function< decltype( shapes::cmp ) >( shapes::cmp ) );


// ---7---output-----
  std::cout << "Vertices: " << quantity.Vertices
            << "\nTriangles: " << quantity.Triangles
            << "\nSquares: " << quantity.Squares
            << "\nRectangles: " << quantity.Rectangles
            << "\nPoints: ";


  std::copy( POINTS.begin(), POINTS.end(), std::ostream_iterator< shapes::Point >( std::cout, " " ) );
  std::cout << "\nShapes:\n";
  
  if ( !SHAPES.empty() ) {
    std::copy( SHAPES.begin(), SHAPES.end(), std::ostream_iterator< shapes::Shape >( std::cout, "\n" ) );
  }

  return 0;
}


void SumVerticesSumShapes::operator()( const shapes::Shape & shape ) { 
    
  Vertices += shape.size();
  shapes::shape_type shape_t = shapes::getShapeType( shape );

  switch ( shape_t ) {
    
    case shapes::TRIANGLE :
      ++Triangles;
      break;

    case shapes::SQUARE :
      ++Squares;
      ++Rectangles;
      break;

    case shapes::RECTANGLE :
      ++Rectangles;
      break;

    default:
      break;
  }
}
