#include "prog_2.h"
#include "Shape.h"
#include <iterator>
#include <algorithm>

void prog_2()
{
  std::vector<std::shared_ptr<Shape>> vec((std::istream_iterator<std::shared_ptr<Shape>>(std::cin)),
                                          std::istream_iterator<std::shared_ptr<Shape>>());
  if (!std::cin && !std::cin.eof()) {
    throw std::runtime_error("Input error.");
  }
  std::cout<<"Original:\n"<<vec;
  std::stable_sort(vec.begin(), vec.end(),
                   std::bind(&Shape::isMoreLeft, std::placeholders::_1, std::placeholders::_2));
  std::cout<<"Left-Right:\n"<<vec;
  std::stable_sort(vec.rbegin(), vec.rend(),
                   std::bind(&Shape::isMoreLeft, std::placeholders::_1, std::placeholders::_2));
  std::cout<<"Right-Left:\n"<<vec;
  std::stable_sort(vec.begin(), vec.end(),
                   std::bind(&Shape::isUpper, std::placeholders::_1, std::placeholders::_2));
  std::cout<<"Top-Bottom:\n"<<vec;
  std::stable_sort(vec.rbegin(), vec.rend(),
                   std::bind(&Shape::isUpper, std::placeholders::_1, std::placeholders::_2));
  std::cout<<"Bottom-Top:\n"<<vec;
}