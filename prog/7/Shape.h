#ifndef LAB_7_SHAPES_H
#define LAB_7_SHAPES_H

#include <iostream>
#include <memory>
#include <vector>

class Shape {
public:
  virtual ~Shape()= default;;
  Shape(int x,int y): x_(x), y_(y){};
  bool isMoreLeft(const std::shared_ptr<Shape> &sh) const;
  bool isUpper(const std::shared_ptr<Shape> &sh) const;
  virtual void draw(std::ostream& os) const =0;
protected:
  int x_,y_;
};

class Circle:public Shape{
public:
  Circle(int x,int y):Shape(x,y){};
  void draw(std::ostream &os) const override;
};

class Square:public Shape{
public:
  Square(int x,int y):Shape(x,y){};
  void draw(std::ostream &os) const override;
};

class Triangle:public Shape{
public:
  Triangle(int x,int y):Shape(x,y){};
  void draw(std::ostream &os) const override;
};

std::istream &operator>>(std::istream &is, std::shared_ptr<Shape> &shapePtr);
std::ostream &operator<<(std::ostream &os, std::vector<std::shared_ptr<Shape>> &vecPtr);

#endif

