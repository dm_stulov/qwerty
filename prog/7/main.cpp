#include "prog_2.h"
#include <iostream>
#include <algorithm>
#include <iterator>
#include <stdexcept>

const double M_PI = 3.14;

int main(int argc, char *argv[]) {
  try {
    if (argc != 2) {
      std::cerr << "Program takes 1 argument.\n";
      return 1;
    }
    switch (atoi(argv[1])) {
      case 1: {
        std::transform(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(),
                       std::ostream_iterator<double>(std::cout, " "), std::bind1st(std::multiplies<double>(), M_PI));
        std::cout << '\n';
        break;
      }
      case 2: {
        prog_2();
        break;
      }
      default: {
        std::cerr << "Unknown argument.\n";
        return 1;
        break;
      }
    }
    return 0;
  }
  catch (const std::runtime_error &er) {
    std::cerr << er.what() << '\n';
    return 2;
  }
  catch (...) {
    std::cerr << "Something Wrong\n";
    return 2;
  }
}