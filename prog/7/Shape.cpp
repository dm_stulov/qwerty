#include "Shape.h"
#include <algorithm>
#include <sstream>
#include <string>
#include <stdc++.hpp>


bool Shape::isMoreLeft(const std::shared_ptr<Shape> &sh) const
{
  return this->x_ < sh->x_;
}

bool Shape::isUpper(const std::shared_ptr<Shape> &sh) const
{
  return this->y_ > sh->y_;
}

void Circle::draw(std::ostream &os) const
{
  os << "CIRCLE " << "(" << x_ << "; " << y_ << ")\n";
}

void Square::draw(std::ostream &os) const
{
  os << "SQUARE " << "(" << x_ << "; " << y_ << ")\n";
}

void Triangle::draw(std::ostream &os) const
{
  os << "TRIANGLE " << "(" << x_ << "; " << y_ << ")\n";
}

std::istream &operator>>(std::istream &is,std::shared_ptr<Shape> &shapePtr)
{
  std::istream::sentry sent(is);
  if(!sent)
  {
    return is;
  }
  std::stringstream source;
  std::string tmp;
  getline(is,tmp);
  source.str(tmp);
  int x, y;
  std::string shape;
  getline(source,shape,'E');
  shape+='E';
  if (!source||source.peek()==EOF) {
    is.setstate(std::ios_base::failbit);
    return is;
  }
  char delim;
  source >> delim;
  if (delim != '(') {
    is.setstate(std::ios_base::failbit);
    return is;
  }
  source >> x;
  if (!source) {
    is.setstate(std::ios_base::failbit);
    return is;
  }
  source >> delim;
  if (delim != ';') {
    is.setstate(std::ios_base::failbit);
    return is;
  }
  source >> y;
  if (!source) {
    is.setstate(std::ios_base::failbit);
    return is;
  }
  source >> delim;
  if (delim != ')' || !source) {
    is.setstate(std::ios_base::failbit);
    return is;
  }
  source.str(shape);
  source>>shape;
  if (shape == "CIRCLE") {
    shapePtr = std::make_shared<Circle>(x, y);
  } else if (shape == "SQUARE") {
    shapePtr = std::make_shared<Square>(x, y);
  } else if (shape == "TRIANGLE") {
    shapePtr = std::make_shared<Triangle>(x, y);
  } else {
    is.setstate(std::ios_base::failbit);
  }
  return is;
}

std::ostream &operator<<(std::ostream &os, std::vector<std::shared_ptr<Shape>> &vecPtr)
{
  std::for_each(vecPtr.begin(), vecPtr.end(), [&](const std::shared_ptr<Shape>& shPtr){shPtr->draw(os); });
  return os;
}
