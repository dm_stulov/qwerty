#include <iostream>
#include <iterator>
#include <algorithm>
#include <stdexcept>

class StatNumbers {
public:

StatNumbers();


void operator()(const long int &num) {

  ++quantity;
  last = num;

  if (num > 0) { ++positive; }

  if (num < 0) { ++negative; }

  if (num % 2) { sumOdd += num; }
  else { sumEven += num; }

  if (empty) {
    empty = false;
    min = num;
    max = num;
    first = num;
    return;
  }

  if (num > max) { max = num; }

  if (num < min) { min = num; }

}

bool Empty() {
  return empty;
}

const long int &Max() const {
  return max;
}

const long int &Min() const {
  return min;
}

long double Mean() {
  return (sumOdd + sumEven) / static_cast<long double>(quantity);
}

const long int &Positive() const {
  return positive;
}

const long int &Negative() const {
  return negative;
}

const long long int &SumOdd() const {
  return sumOdd;
}

const long long int &SumEven() const {
  return sumEven;
}

bool EqFirstLast() {
  return (first == last);
}

private:
bool empty;
long int max{};
long int min{};
long int first{};
long int last{};
long int quantity;
long int positive;
long int negative;
long long int sumOdd;
long long int sumEven;

};

StatNumbers::StatNumbers() : empty(true),
                             quantity(0),
                             positive(0),
                             negative(0),
                             sumOdd(0),
                             sumEven(0) {}

int main(int, char const *[]) {
  try {
    StatNumbers stat = std::for_each(std::istream_iterator<long int>(std::cin), std::istream_iterator<long int>(),
                                     StatNumbers());

    if (!std::cin.eof()) {
      std::cerr << "Error: fail input data\n";
      return 1;
    }

    if (stat.Empty()) {
      std::cout << "No Data\n";
      return 0;
    }

    std::cout.precision(1);
    std::cout << "Max: " << stat.Max() << "\n"
              << "Min: " << stat.Min() << "\n"
              << "Mean: " << std::fixed << stat.Mean() << "\n"
              << "Positive: " << stat.Positive() << "\n"
              << "Negative: " << stat.Negative() << "\n"
              << "Odd Sum: " << stat.SumOdd() << "\n"
              << "Even Sum: " << stat.SumEven() << "\n"
              << "First/Last Equal: " << (stat.EqFirstLast() ? "yes" : "no") << "\n";

    return 0;

  } catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
  } catch (...) {
    std::cerr << "Unknown error\n";
  }

  return 2;
}
