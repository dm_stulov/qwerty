#ifndef WORD_MACHINE_HPP
#define WORD_MACHINE_HPP

#include <iosfwd>
#include <string>
#include <vector>

class WordMachine 
{

public:
  WordMachine( std::istream & in );

  std::string getTextUnit();

private:

  enum States 
  {
    State_Start,
    State_Word,
    State_Number,
    State_Double,
    State_Done,
    State_Error
  };

  static const size_t STATES_COUNT = 6;

  enum Events 
  {
    Event_EOF,
    Event_Space,
    Event_Alpha,
    Event_Digit,
    Event_Hyphen,
    Event_DecPoint,
    Event_Dash,
    Event_Punct
  };

  static const size_t EVENTS_COUNT = 8;


  void process( );

  typedef void ( WordMachine::*Action )( char );


  Events getEvent( char );
  
  struct Transition
  {
    Transition();
    Transition( States targetState, Action action );

    States TargetState_;
    Action Action_;
  };

  void addTransition( States fromState, States toState, Events event );
  void addTransition( States fromState, States toState, Events event, Action action );

  typedef std::vector< Transition > TransitionByEventsTable;
  typedef std::vector< TransitionByEventsTable > StatesTable;

  std::istream * in_;
  std::string textUnit_;

  StatesTable States_;
  States CurrentState_;

  char decimal_point;


  void doNothing( char );
  void add( char c );
  void addDash( char );
  void unget( char );
  void ungetDash( char );
};

#endif
