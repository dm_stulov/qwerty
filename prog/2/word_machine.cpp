#include "word_machine.hpp"

#include <iosfwd>
#include <sstream>
#include <cctype>
#include <locale>
#include <stdexcept>


std::string WordMachine::getTextUnit()
{
  textUnit_.clear();
  process();
  return textUnit_;
}

WordMachine::WordMachine( std::istream & in )
    : in_(&in), 
    textUnit_(),
    CurrentState_( WordMachine::State_Start )
{

  const std::numpunct <char> & npunct = std::use_facet < std::numpunct <char> >( in.getloc() );
  decimal_point = npunct.decimal_point();


  States_.resize( STATES_COUNT );
  for ( size_t i = 0; i != STATES_COUNT; ++ i ) {
    States_[i].resize( EVENTS_COUNT );
    WordMachine::TransitionByEventsTable & transitionByEvents = States_[i];
    for ( size_t j = 0; j != EVENTS_COUNT; ++j ) {
      transitionByEvents[i] = Transition( static_cast<States>(i), &WordMachine::doNothing );
    }
  } 

//  using namespace WordMachine;

  WordMachine::addTransition( State_Start,    State_Done,   Event_EOF,      &WordMachine::add );
  WordMachine::addTransition( State_Start,    State_Start,  Event_Space );
  WordMachine::addTransition( State_Start,    State_Word,   Event_Alpha,    &WordMachine::add );
  WordMachine::addTransition( State_Start,    State_Number, Event_Digit,    &WordMachine::add );
  WordMachine::addTransition( State_Start,    State_Done,   Event_Punct,    &WordMachine::add );
  WordMachine::addTransition( State_Start,    State_Done,   Event_Hyphen,   &WordMachine::add );
  WordMachine::addTransition( State_Start,    State_Done,   Event_DecPoint, &WordMachine::add );
  WordMachine::addTransition( State_Start,    State_Done,   Event_Dash,     &WordMachine::addDash );

  WordMachine::addTransition( State_Word,     State_Done,   Event_EOF,      &WordMachine::unget );
  WordMachine::addTransition( State_Word,     State_Done,   Event_Space );
  WordMachine::addTransition( State_Word,     State_Word,   Event_Alpha,    &WordMachine::add );
  WordMachine::addTransition( State_Word,     State_Error,  Event_Digit,    &WordMachine::unget );
  WordMachine::addTransition( State_Word,     State_Done,   Event_Punct,    &WordMachine::unget );
  WordMachine::addTransition( State_Word,     State_Word,   Event_Hyphen,   &WordMachine::add );
  WordMachine::addTransition( State_Word,     State_Done,   Event_DecPoint, &WordMachine::unget );
  WordMachine::addTransition( State_Word,     State_Done,   Event_Dash,     &WordMachine::ungetDash );

  WordMachine::addTransition( State_Number,   State_Done,   Event_EOF,      &WordMachine::unget );
  WordMachine::addTransition( State_Number,   State_Done,   Event_Space );
  WordMachine::addTransition( State_Number,   State_Error,  Event_Alpha,    &WordMachine::unget );
  WordMachine::addTransition( State_Number,   State_Number, Event_Digit,    &WordMachine::add );           
  WordMachine::addTransition( State_Number,   State_Done,   Event_Punct,    &WordMachine::unget );
  WordMachine::addTransition( State_Number,   State_Done,   Event_Hyphen,   &WordMachine::unget );
  WordMachine::addTransition( State_Number,   State_Double, Event_DecPoint, &WordMachine::add );          
  WordMachine::addTransition( State_Number,   State_Done,   Event_Dash,     &WordMachine::ungetDash );

  WordMachine::addTransition( State_Double,   State_Done,   Event_EOF,      &WordMachine::unget );
  WordMachine::addTransition( State_Double,   State_Done,   Event_Space );
  WordMachine::addTransition( State_Double,   State_Error,  Event_Alpha,    &WordMachine::unget );         
  WordMachine::addTransition( State_Double,   State_Double, Event_Digit,    &WordMachine::add );
  WordMachine::addTransition( State_Double,   State_Done,   Event_Punct,    &WordMachine::unget );
  WordMachine::addTransition( State_Double,   State_Done,   Event_Hyphen,   &WordMachine::unget );
  WordMachine::addTransition( State_Double,   State_Done,   Event_DecPoint, &WordMachine::unget );      
  WordMachine::addTransition( State_Double,   State_Done,   Event_Dash,     &WordMachine::ungetDash );

}



void WordMachine::process() 
{
  CurrentState_ = WordMachine::State_Start;
  std::istream::sentry sentry( *in_ );
  if ( sentry ) {
    textUnit_.clear();

    while ( ( CurrentState_ != WordMachine::State_Done ) && ( CurrentState_ != WordMachine::State_Error ) ) {

      char nextChar = in_->get();

      TransitionByEventsTable & transitionByEvents = States_[ CurrentState_ ];
      Transition & transition = transitionByEvents[ getEvent( nextChar ) ];

      CurrentState_ = transition.TargetState_;
      ( this->*( transition.Action_ ) )( nextChar );
    }

    if ( CurrentState_ == WordMachine::State_Error ) {
      std::ostringstream message;
      std::string errorUnit;
      *in_ >> errorUnit;
      message << "Error: invalid text: " << textUnit_ << errorUnit; 
      throw std::invalid_argument( message.str() );
    }
  }
}

void WordMachine::addTransition( States fromState, States toState, Events event )
{
  WordMachine::TransitionByEventsTable & transitionByEvents = States_[ fromState ];
  transitionByEvents[ event ] = Transition( toState, &WordMachine::doNothing );
}

void WordMachine::addTransition( States fromState, States toState, Events event, Action action )
{
  TransitionByEventsTable & transitionByEvents = States_[ fromState ];
  transitionByEvents[ event ] = Transition( toState, action );
}

WordMachine::Transition::Transition() 
{}

WordMachine::Transition::Transition( States targetState, Action action ) 
    : TargetState_( targetState ),
    Action_( action ) 
{}

WordMachine::Events WordMachine::getEvent( const char c ) 
{
  if ( c == std::char_traits<char>::eof() ) {
    return WordMachine::Event_EOF;
  
  } else if ( std::isalpha( c ) ) {
    return WordMachine::Event_Alpha;

  } else if ( std::isdigit( c ) ) {
    return WordMachine::Event_Digit;

  } else if ( std::isspace( c ) ) {
    return WordMachine::Event_Space;

  } else if ( ( c == '-' ) && ( in_->peek() != '-' ) ) {
    return WordMachine::Event_Hyphen;
  
  } else if ( ( c == decimal_point ) && std::isdigit( in_->peek() ) ) {
    return WordMachine::Event_DecPoint;
  
  } else if ( ( c == '-' ) && ( in_->peek() == '-' ) ) {
    if ( ( in_->get() == '-' ) && ( in_->peek() == '-' ) ) {
      in_->get();
      return WordMachine::Event_Dash;
    } else {
      in_->unget();
      throw std::invalid_argument( "Error: unknown char" );
    }
  
  } else if ( std::ispunct( c ) ) {
    return WordMachine::Event_Punct;
  }

  in_->unget();
  throw std::invalid_argument( "Error: unknown char" );
}

void WordMachine::doNothing( char ) {}

void WordMachine::add( char c ) {
  textUnit_.push_back( c );
}

void WordMachine::addDash( char ) {
  textUnit_ += "---";
}

void WordMachine::unget( char ) {
  in_->unget();
}

void WordMachine::ungetDash( char ) {
  in_->putback('-');
  in_->putback('-');
  in_->putback('-');
}
