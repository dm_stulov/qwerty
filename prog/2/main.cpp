#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include <cstring>

int printByLine( std::istream &, std::ostream &, int max_length );

const char Invalid_argument[] = "Invalid argument, expected --line-width int( > 24)\n";
const int MIN_LENGHT = 24;


int main(int argc, char const *argv[])
{
  try {

    if ( ( argc != 1 ) && ( argc != 3 ) ) {
      std::cerr << Invalid_argument;
      return 1;
    } 
    
    int max_lenght;

    const char key[] = "--line-width";
    
    if ( argc == 3 ) {
      if ( !( std::strcmp( argv[1], key ) ) && ( std::atoi( argv[2] ) > MIN_LENGHT ) ) {
        max_lenght = (size_t)std::atoi( argv[2] );
      } else {
        std::cerr << Invalid_argument;
        return 1;
      }
    } else {
      max_lenght = 40;
    }

    return printByLine( std::cin, std::cout, max_lenght );

    
  } catch ( const std::exception & e ) {
    std::cerr << "Incorrect input\n";
    return 1;
  }
  return 0;

}
