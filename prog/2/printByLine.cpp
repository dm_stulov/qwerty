#include "word_machine.hpp"

#include <iosfwd>
#include <exception>
#include <cctype>
#include <list>
#include <iterator>
#include <algorithm>

enum UnitType {
  ALNUM,
  DASH,
  PUNCT
};

UnitType getUnitType( const std::string & s );

const int MAX_SIZE_WORD = 20;

int printByLine( std::istream & in, std::ostream & out, int max_lenght )
{
  in >> std::ws;
  if ( in.eof() ) {
    return 0;
  }

  WordMachine m( in );
  
  std::string line;

  std::string str;
  UnitType lastType = PUNCT;
  
  while ( !in.eof() && !( str = m.getTextUnit()).empty() ) { 

    // читаем единицу текста 
    // и проверяем на корректность сочетания последней и предпоследней единицы
    
    if ( ( str.size() > MAX_SIZE_WORD ) || ( (int)str.size() > max_lenght ) ) {
      throw std::exception();
    }

    if ( ( getUnitType(str) == PUNCT ) && ( lastType != ALNUM ) ) {
      throw std::exception();
    }

    if ( ( getUnitType(str) == DASH ) && ( lastType == DASH ) ) {
      throw std::exception();
    }

    if ( ( getUnitType(str) == DASH ) && ( lastType == PUNCT ) && ( line.back() != ',' ) ) {
      throw std::exception();
    }

    lastType = getUnitType( str );


    // лепим в строку

    if ( line.empty() || lastType == PUNCT ) {
      line += str;
    } else {
      line += " " + str;
    }

    // печатаем что нужно ( если нужно ) 

    if ( (int)line.size() > max_lenght ) {
      size_t end = line.rfind( ' ' );

      if ( getUnitType( line.substr( end + 1 ) ) == DASH ) {
        end = line.rfind( " ", ( end - 1 ) );
      }

      out << line.substr( 0, end ) << "\n";
      line.erase( 0, ( end + 1 ) );
    }
  }

  if ( line.empty() ) {
    return 0;
  }

  out << line << "\n";

  return 0;
}


UnitType getUnitType( const std::string & s )
{

  if (  s.size() == 3 ) {
    if ( ( s[0] == '-' ) && ( s[1] == '-' ) && ( s[2] == '-' ) ) {
      return DASH;
    }
  }

  if ( std::isalpha( s[0] ) || std::isdigit( s[0] )) {
    return ALNUM;

  } else if ( std::ispunct( s[0] ) ) {
    return PUNCT;
  }
  throw std::exception();
}
